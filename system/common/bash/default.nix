{ config, pkgs, lib, ... }:

{
  programs.bash = {
    interactiveShellInit = builtins.readFile ./config;
    #promptInit = ''PS1="\h\\$ "'';
    shellAliases = {
      c      = "clear";
      ls     = "ls -CF --color=auto";
      ll     = "s -lisa --color=auto";
      free   = "free -mt";
      ps     = "ps auxf";
      psgrep = "ps aux | grep -v grep | grep -i -e VSZ -e";
      wget   = "wget -c";
      histg  = "history | grep";
      myip   = "curl ipv4.icanhazip.com";
      grep   = "grep --color=auto";
      info   = "info --vi-keys";
    };
  };
}
