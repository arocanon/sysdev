{ pkgs, lib, ... }:

{
  programs.tmux = {
    enable = true;
    extraConfig = ''
      run-shell ${pkgs.tmuxPlugins.resurrect}/share/tmux-plugins/resurrect/resurrect.tmux
    '';
    plugins = with pkgs.tmuxPlugins; [
      resurrect
      vim-tmux-navigator # broken
    ];
  };
  environment.etc."tmux.conf".source = lib.mkForce ./config;
}

