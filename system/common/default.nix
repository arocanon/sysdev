# configuration.nix
{ config, lib, pkgs, nixpkgs, ... }:

{
  # Enable flakes
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # customize kernel version
  imports = [
    ../kernel/default.nix
    git/default.nix
    tmux/default.nix
    vim/default.nix
    bash/default.nix
  ];
  
  users.groups.admin = {};
  users.users = {
    admin = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      password = "admin";
      group = "admin";
    };
  };

  # Members of the tracing group can use lttng kernel events without root permissions
  users.groups.tracing.members = [ "admin" ];

  security.sudo.extraConfig = ''
    # Forward ssh-agent env to allow calling nixos-rebuild with sudo
    Defaults    env_keep+=SSH_AUTH_SOCK
  '';

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "Europe/Madrid";

  # Easiest to use and most distros use this by default.
  networking.networkmanager.enable = true;

  environment.variables = {
    EDITOR = "vim";
    VISUAL = "vim";
  };

  nix.nixPath = [
    "system=${nixpkgs}"
    "nixpkgs=${nixpkgs}"
  ];

  nix.registry.system.flake = nixpkgs;

  # fundamental system packages
  environment.systemPackages = with pkgs; [
    btop
    lolcat
    cowsay

    trace-cmd
    bpftrace
  ];
}
