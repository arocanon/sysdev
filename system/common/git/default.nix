{ pkgs, ... }:

{
  #programs.git.enable = true;
  environment.systemPackages = with pkgs; [ git ];
  environment.etc."gitconfig".source = ./config;
}
