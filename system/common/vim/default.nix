{ pkgs, nixpkgs, ... }:

let
  config = (builtins.readFile ./config.vim) + (builtins.readFile ./cscope_maps.vim);

  myvim = with pkgs; (vim_configurable.override { }).customize {
    vimrcConfig.packages.myplugins = with pkgs.vimPlugins; {
      start = [
        vim-nix
        vim-airline
        vim-tmux-navigator
        nerdtree
        tagbar
        unicode-vim
        gruvbox
        fugitive

        # not found in nixpkgs
        #vim-linux-coding-syle
      ] ++ pkgs.myVimPlugins;
      opt = [];
    };
    vimrcConfig.customRC = config;
  };

  overlay = final: prev: {
    myVimPlugins = prev.myVimPlugins or []; 
  };

in {
  environment.systemPackages = with pkgs; [
    myvim
  ];

  nixpkgs.overlays = [ overlay ];
}

