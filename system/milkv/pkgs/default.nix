{ nixpkgs, eupilotOverlay, pkgs, ...}:

let
  overlay = final: prev: {
    kgdbSetup = final.callPackage misc/kgdb-setup.nix {};
    lockstat = final.callPackage misc/lockstat.nix {};

    nosvLatest = (final.nosv.override {
      useGit = true;
      gitCommit = "3ca2f67993f85aa73c53f810ff12148189eae642";
      gitBranch = "master";
      #gitUrl = "git@gitlab-internal.bsc.es:arocanon/nos-v.git";
      gitUrl = "git@gitlab-internal.bsc.es:nos-v/nos-v.git";
    }).overrideAttrs(old: {
      # TODO: how do we automatically find this in risc-v
      CACHELINE_WIDTH = 64;
    });

    nosv = prev.nosv.overrideAttrs(old: {
      # TODO: how do we automatically find this in risc-v
      CACHELINE_WIDTH = 64;
      patches = (old.patches or []) ++ [
        ./patches/nosv/riscv.patch
      ];
    });

    nodesLatest = prev.nodes.override {
      useGit = true;
      gitCommit = "7a27c48901cd0a44562428e1492f8208e9b422dd";
      gitBranch = "master";
      gitUrl = "git@gitlab-internal.bsc.es:nos-v/nodes.git";
    };

    nodes = prev.nodes.overrideAttrs(old: {
      patches = (old.patches or []) ++ [
        ./patches/nodes/boost.patch
      ];
    });

    pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [(
      python-final: python-prev: rec {
        #See https://hypothesis.readthedocs.io/en/latest/healthchecks.html for
        #more information about this. If you want to disable just this health
        #check, add HealthCheck.too_slow to the suppress_health_check settings
        #for this test.
        hypothesis = python-prev.hypothesis.overrideAttrs(old: {
          disabledTests = (old.disabledTests or []) ++ [
            "test_observability_captures_stateful_reprs"
          ];
        });
        # open issue https://github.com/NixOS/nixpkgs/issues/275626
        numpy = python-prev.numpy.overridePythonAttrs (old: {
          disabledTests = (old.disabledTests or []) ++ [
            "test_umath_accuracy"
            "TestAccuracy::test_validate_transcendentals"
            "test_validate_transcendentals"
          ];
        });
      }
    )];




  };
in {
  nixpkgs.overlays = [
    eupilotOverlay
    overlay
  ];

  environment.systemPackages = with pkgs; [
    # nothing here for now
  ];

  system.userActivationScripts = {
    pkgsSetup = ''
      bindir=/home/admin/bin
      rm -rf $bindir
      mkdir -p $bindir

      ## Misc packages
      #ln -s ${pkgs.kgdbSetup} $bindir/kgdb-setup.sh
      #ln -s ${pkgs.lockstat} $bindir/lockstat.sh

      ## FCS packages
      #mkdir -p $bindir/fcs
      #ln -s ${pkgs.fcsBasic}          $bindir/fcs/fcs-basic
      #ln -s ${pkgs.fcsPipe}           $bindir/fcs/fcs-pipe
      #ln -s ${pkgs.expPipeFcs}        $bindir/fcs/exp-pipe-fcs
      #ln -s ${pkgs.expNestedNodesFcs} $bindir/fcs/exp-nested-nodes-fcs

      # EUPilot packages
      mkdir -p $bindir/ompv
      ln -s ${pkgs.exp-fcs-nested-omp-free-agents} $bindir/ompv/exp-nested
      ln -s ${pkgs.exp-fcs-fib-omp-free-agents}    $bindir/ompv/exp-fib
      ln -s ${pkgs.exp-fcs-heat-omp-free-agents}   $bindir/ompv/exp-heat
      ln -s ${pkgs.exp-fcs-sparselu}   $bindir/ompv/exp-sparselu
      ln -s ${pkgs.tst-bots-ompv}   $bindir/ompv/bots
    '';
  };
}
