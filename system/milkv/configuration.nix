{ modulesPath, config, lib, pkgs, ... }:

{
  imports = [
    ../common/default.nix
    ./pkgs/default.nix
  ];

  ec2.efi = true;

  # We have a very small EFI parittion. Set a small kernel limit.
  boot.loader.grub.configurationLimit = 2;

  networking.hostName = "milkv";

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
  };

  users.users."admin".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF3zeB5KSimMBAjvzsp1GCkepVaquVZGPYwRIzyzaCba aleix@bsc"
  ];

  # passwordless sudo
  security.sudo.wheelNeedsPassword = false;

  environment.systemPackages = with pkgs; [
    openfortivpn
  ];

  boot.kernelParams = [
    "nokaslr"
  ];

  #programs.bash.loginShellInit = ''
  #  ${pkgs.figlet}/bin/figlet "Welcome to Tofu"  | ${pkgs.lolcat}/bin/lolcat
  #'';


  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?
}

