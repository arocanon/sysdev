{ system, pkgs, lib, ... }:

let
  kernel = nixosDevKernel;
  #kernel = nixos-fcs-6_9_6;

  nixos-kernel = lib.makeOverridable ({version, gitCommit, lockStat ? false, lockDep ? false, preempt ? false, debug ? false, kgdb ? false, branch ? "fcs"}: 
  let
    dbg = if (kgdb) then true else debug;
  in pkgs.linuxPackagesFor (pkgs.buildLinux rec {
    inherit version;
    src = builtins.fetchGit {
      url = "git@gitlab-internal.bsc.es:arocanon/linux.git";
      rev = gitCommit;
      ref = branch;
    };
    structuredExtraConfig = with lib.kernel; {
      LOCALVERSION_AUTO = no;
      MAGIC_SYSRQ = yes;
      MAGIC_SYSRQ_DEFAULT_ENABLE = freeform "0x1"; # Enable all sysrq codes
      PSI_DEFAULT_DISABLED = yes; # TODO fix psi subsystem
    } // lib.optionalAttrs lockStat {
      LOCK_STAT = yes;
    } // lib.optionalAttrs lockDep {
      PROVE_LOCKING = yes;
    } // lib.optionalAttrs preempt {
      PREEMPT = lib.mkForce yes;
      PREEMPT_VOLUNTARY = lib.mkForce no;
    } // lib.optionalAttrs dbg {
      UNWINDER_FRAME_POINTER = yes;
      DEBUG_INFO = yes;
    } // lib.optionalAttrs (kgdb) ({
        # Enable gdb python scripts to debug the kernel
        GDB_SCRIPTS = yes;
        # Enable kgdb over serial console
        KGDB = yes;
        KGDB_SERIAL_CONSOLE = yes;
        # Enable sysrq-g over serial to break into the kernel. configure gdb with "set remote interrupt-sequence BREAK-g"
        MAGIC_SYSRQ_SERIAL = yes;
        MAGIC_SYSRQ_SERIAL_SEQUENCE = freeform "g";

        #DEBUG_DRIVER = yes;
        # not supported in arm64. Use hardware breakpoints (hbreak) instead.
        #STRICT_KERNEL_RWX = no;
      } // lib.optionalAttrs (system == "aarch64-linux") {
        # Allows gdb to unwind backtraces on arm64
        # https://lore.kernel.org/kernelnewbies/040f01d85a17$eb246c10$c16d4430$@etri.re.kr/T/
        ARM64_PTR_AUTH = no;
        ARM64_PTR_AUTH_KERNEL = no;
    });
    kernelPatches = [];
    extraMeta.branch = lib.versions.majorMinor version;
  }));

  nixosDevKernel = nixos-kernel {version="6.9.6"; branch="fcs"; kgdb = true; gitCommit = "0dca5f251e712b0813e4d4b24fe1ffb544e772f5";};
  nixos-fcs-bypass-6_9_6 = nixos-kernel {version="6.9.6"; branch="refs/tags/fcs-6.9.6-v1.1.1"; kgdb = true; gitCommit = "720c1d7935a2e8644dee4d9b1253369b1b592f66";};
  nixos-fcs-6_9_6 = nixos-kernel {version="6.9.6"; branch="refs/tags/fcs-6.9.6-v1.1.0"; kgdb = false; gitCommit = "6097edca1e81201a20e6fe9b6a16955cc09a1165";};
  latest = pkgs.linuxPackages_latest;


  ## Other Linux setups
  #
  #fcs-devel = pkgs.linuxPackages_custom {
  #   version = "6.2.8";
  #   src = /mnt/data/kernel/fcs/kernel/src;
  #   configfile = /mnt/data/kernel/fcs/kernel/configs/defconfig;
  #};

  #fcs-kernel = gitCommit: lockdep: pkgs.linuxPackages_custom {
  #   version = "6.2.8";
  #   src = builtins.fetchGit {
  #     url = "git@bscpm03.bsc.es:ompss-kernel/linux.git";
  #     rev = gitCommit;
  #     ref = "fcs";
  #   };
  #   configfile = if lockdep then ./configs/lockdep else ./configs/defconfig;
  #};
in {
  imports = [
    #./lttng.nix
    ./perf.nix
  ];
  boot.kernelPackages = lib.mkForce kernel;

  # disable all cpu mitigations
  boot.kernelParams = [
    "mitigations=off"
  ];

  # default loglevel
  boot.consoleLogLevel = 4;

  # enable memory overcommit, needed to build a taglibc system using nix after
  # increasing the openblas memory footprint
  boot.kernel.sysctl."vm.overcommit_memory" = 1;
}
