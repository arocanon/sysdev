# configuration.nix
{ config, lib, pkgs, ... }: {

  # customize kernel version
  imports = [
    ../common/default.nix
    ../kernel/default.nix
    ../pkgs/default.nix
  ];

  # auto login on boot
  services.getty.autologinUser = "admin";

  # passwordless sudo
  security.sudo.wheelNeedsPassword = false;

  virtualisation.vmVariant = {
    # following configuration is added only when building VM with build-vm
    virtualisation = {
      memorySize = 8 * 1024; # In MiB
      cores = 14;
      graphics = false;
    };
  };

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = true;
  };

  networking.hostName = "eda";
  networking.firewall.allowedTCPPorts = [ 22 ];

  system.stateVersion = "23.05";
}
