{writeShellScript}:

writeShellScript "kgdb-setup.sh" ''
  echo "ttyS0,115200" | sudo tee /sys/module/kgdboc/parameters/kgdboc 

  # disable kgdb
  # echo "" | sudo tee /sys/module/kgdboc/parameters/kgdboc 

  echo "g" | sudo tee /proc/sysrq-trigger
''
