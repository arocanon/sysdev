{ nixpkgs, nixdevOverlay, eupilot, pkgs, ...}:

let
  overlay = final: prev: {
    kgdbSetup = final.callPackage misc/kgdb-setup.nix {};
    lockstat = final.callPackage misc/lockstat.nix {};

    tst-bots-ompv = eupilot.tst-bots-ompv;
    eupilotHello = eupilot.hello;
  };
in {
  nixpkgs.overlays = [
    nixdevOverlay
    overlay
  ];

  environment.systemPackages = with pkgs; [
    # nothing here for now
  ];

  system.userActivationScripts = {
    pkgsSetup = ''
      bindir=/home/admin/bin
      rm -rf $bindir
      mkdir -p $bindir

      # Misc packages
      ln -s ${pkgs.kgdbSetup} $bindir/kgdb-setup.sh
      ln -s ${pkgs.lockstat} $bindir/lockstat.sh

      # FCS packages
      mkdir -p $bindir/fcs
      ln -s ${pkgs.fcsBasic}          $bindir/fcs/fcs-basic
      ln -s ${pkgs.fcsPipe}           $bindir/fcs/fcs-pipe
      ln -s ${pkgs.expPipeFcs}        $bindir/fcs/exp-pipe-fcs
      ln -s ${pkgs.expNestedNodesFcs} $bindir/fcs/exp-nested-nodes-fcs

      # Bots packages
      ln -s ${pkgs.botsInput}      $bindir/bots-input
      ln -s ${pkgs.botsSched}      $bindir/bots-sched
      ln -s ${pkgs.botsSchedCheck} $bindir/bots-sched-check
      ln -s ${pkgs.expBotsSched}   $bindir/exp-bots-graviton

      # EUPilot packages
      mkdir -p $bindir/ompv
      ln -s ${eupilot.exp-fcs-nested-omp-free-agents} $bindir/ompv/exp-nested
      ln -s ${eupilot.exp-fcs-fib-omp-free-agents}    $bindir/ompv/exp-fib
      ln -s ${eupilot.exp-fcs-heat-omp-free-agents}   $bindir/ompv/exp-heat
      ln -s ${eupilot.exp-fcs-sparselu}   $bindir/ompv/exp-sparselu

      ln -s ${eupilot.tst-bots-ompv}   $bindir/ompv/bots
    '';
  };
}
