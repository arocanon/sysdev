# configuration.nix
{ config, lib, pkgs, ... }: let
  arch = "sapphirerapids";
in {

  imports = [
    ../common/default.nix
    ../kernel/default.nix
    ../pkgs/default.nix
  ];

  networking.hostName = "mn5";

  #nixpkgs.buildPlatform = {
  #  system = "x86_64-linux";
  #};

  nixpkgs.hostPlatform = {
    gcc.arch = arch;
    gcc.tune = arch;
    system = "x86_64-linux";
  };

  nixpkgs.buildPlatform = {
    gcc.arch = "native";
    gcc.tune = "native";
    system = "x86_64-linux";
  };


  system.stateVersion = "23.05";
}
