define cfs_rq
    print & $lx_per_cpu("runqueues").cfs
end

# Breakpoints
#b kernel/sched/core.c:9768
#b kernel/sched/core.c:9772
#b kernel/sched/core.c:9817
#b kernel/sched/fair.c:835

# Print all to screen
set pagination off

# Print array elements on per line
set print array on

# Print structs prettier
set print pretty on


# Tun on logging
set logging file gdb.log
set logging enabled on
set logging overwrite on

# Send sysrq-g when typing ctrl-c on gdb
# I think that it only works if using a real serial cable to trigger a real hardware interrupt
# It is also possible to send a sysrq-g over ssh by typing the scape sequence: ~Bg (you can check it with ~Bh, which displays sysrq [h]elp command)
#set remote interrupt-sequence BREAK-g
