import sys
import json
import array
import logging
import os
from pprint import pprint
from collections import defaultdict

log = logging.getLogger("gdbcmds")

# load linux python bindings
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../build/scripts/gdb")

from linux import rbtree
from linux import cpus
from linux import utils
from linux import tasks as tasks_module


def elem_list_to_value(lst):
	"""Given a Python list of gdb.Value elements, it returns a gdb.Value array
	with the same length as the python list with a gdb.Value pointer to each element."""

	result = None
	num_elems = len(lst)
	if (num_elems):
		# get the node type
		elem_type = lst[0].type
		# get a node pointer type
		elem_ptr_type = elem_type.pointer()
		# create a gdb type that represents a C array of nodes
		elem_array_type = elem_ptr_type.array(num_elems - 1)
		# get a list of the address of each node in python int format
		addresses = list(map(lambda e: int(str(e.address), 16), lst))
		# allocate memory to store the addresses in the form of an array of pointers
		mem = array.array('Q', addresses)
		# create gdb.Value
		result = gdb.Value(mem, elem_array_type)
	return result

def ptr_list_to_value(lst):
	"""Given a Python list of gdb.Value pointer elements, it returns a gdb.Value array
	with the same length as the python list with a gdb.Value pointer to each element."""

	result = None
	num_elems = len(lst)
	if (num_elems):
		# get the node type
		elem_ptr_type = lst[0].type
		# create a gdb type that represents a C array of nodes
		elem_array_type = elem_ptr_type.array(num_elems - 1)
		# get a list of the address of each node in python int format
		addresses = list(map(lambda e: int(str(e), 16), lst))
		# allocate memory to store the addresses in the form of an array of pointers
		mem = array.array('Q', addresses)
		# create gdb.Value
		result = gdb.Value(mem, elem_array_type)
	return result

def array_of_pointers_to_list(array_value):
	pointer_type = array_value.type.target().pointer()  # Get the type of the pointers
	array_size = array_value.type.sizeof  # Size of the whole array (in bytes)
	pointer_size = pointer_type.sizeof  # Size of each pointer
	num_elements = array_size // pointer_size

	pointer_list = []
	for i in range(num_elements):
		pointer_address = array_value[i];
		pointer_list.append(pointer_address)
	return pointer_list

def threads_of_task(task):
	"""Retruns a list with all threads of task."""

	threads = []
	group_leader = task["group_leader"]
	all_tasks = tasks_module.task_lists();
	for curr in all_tasks:
		if curr["group_leader"] == group_leader:
			threads.append(curr)

	return threads

def entity_is_task(se):
	return not se["my_q"]

def rb_walk(root):
	"""Returns a python list of gdb.Value representing "struct rb_node". """

	nodes = []
	result = None

	node = rbtree.rb_first(root)
	while node:
		nodes.append(node)
		node = rbtree.rb_next(node)
	return nodes

def cfs_rq_of(cpu):
	var_ptr = gdb.parse_and_eval("&runqueues")
	rq = cpus.per_cpu(var_ptr, cpu)
	cfs = rq["cfs"]
	return cfs;

def rb_tree(cfs_rq):
	log.debug("rb_tree")
	result = []
	root = cfs_rq["tasks_timeline"]["rb_root"]
	nodes = rb_walk(root)
	task_struct_ptr_type = gdb.lookup_type("struct task_struct").pointer()
	log.debug(f"task_struct_ptr_type: {task_struct_ptr_type}")
	sched_entity_ptr_type = gdb.lookup_type("struct sched_entity").pointer()
	log.debug(f"sched_entity_ptr_type: {sched_entity_ptr_type}")
	for node in nodes:
		log.debug(f"node: {node}")
		se = utils.container_of(node, sched_entity_ptr_type, "run_node")
		log.debug(f"se: {se}")
		if (not se):
			log.error("Warning se is NULL")
			continue
		if entity_is_task(se):
			ts = utils.container_of(se, task_struct_ptr_type, "se")
			log.debug(f"ts: {ts}")
			result.append(ts)
		else:
			log.debug(f"subtree!")
			tasks = rb_tree(se["my_q"])
			if (tasks):
				result.append(tasks)
	log.debug(f"result: {result}")
	return result

class LxRbWalk (gdb.Function):
	""" Returns all nodes in a rbtree.

$lx_rb_walk(root): Return all nodes of the provided struct rb_root *. """

	def __init__(self):
		super (LxRbWalk, self).__init__("lx_rb_walk")
	def invoke (self, root):
		result = rb_walk(root);
		if result is None:
			raise gdb.GdbError("No entry in tree")
		return elem_list_to_value(result)

LxRbWalk()

class LxCfs (gdb.Function):
	""" Return the current cfs rq.

$lx_cfs_rq([cpu]) returns a pointer to the cfs_rq of the provided cpu, or the current one if no cpu is provided."""

	def __init__(self):
		super (LxCfs, self).__init__("lx_cfs_rq")
	def invoke (self, cpu = -1):
		return cfs_rq_of(cpu);

LxCfs()

class LxCfsTree (gdb.Function):
	""" Print the tasks inside the current cfs_rq.

$lx_cfs_tree([cpu]) returns all tasks in the cfs_rq of the provided cpu, or the current one if no cpu is provided."""

	def __init__(self):
		super (LxCfsTree, self).__init__("lx_cfs_tree")
	def invoke (self, cpu = -1):
		cfs_rq = cfs_rq_of(cpu)
		result = rb_tree(cfs_rq);
		if not result:
			raise gdb.GdbError("No entry in tree")
		return ptr_list_to_value(result)

LxCfsTree()

class LxTaskOf (gdb.Function):
	""" Return the struct task_struct of an struct sched_entity.

$lx_task_of(se) returns a pointer of the struct task_struct of the se pointer if se is a task."""

	def __init__(self):
		super (LxTaskOf, self).__init__("lx_task_of")
	def invoke (self, se):
		if not se:
			return None
		if not entity_is_task(se):
			log.info("Entity is not a task")
			return None
		task_struct_ptr_type = gdb.lookup_type("struct task_struct").pointer()
		return utils.container_of(se, task_struct_ptr_type, "se")

LxTaskOf()

class LxThreads (gdb.Function):
	""" Returns all threads that belong to the same address space as the provided thread.

$lx_threads(pid): Return all threads of pid. """

	def __init__(self):
		super (LxThreads, self).__init__("lx_threads")
	def invoke (self, pid):
		task = tasks_module.get_task_by_pid(pid)
		if not task:
			raise gdb.GdbError("No task of PID " + str(pid))
		else:
			result = threads_of_task(task);
			if result is None:
				raise gdb.GdbError("Unexpected error")
			return ptr_list_to_value(result)

LxThreads()

class PrintElements(gdb.Function):
	""" Given an array of pointer, it prints the specified fields of each element.

$print_elements(array, members): For each element in "array", return all properties in the "members" list. The members list must be a string of properties separated by spaces.
Examples:
	print_elements($lx_threads(0), "pid se->cfs_rq")
"""
	def __init__(self):
		super (PrintElements, self).__init__("print_elements")
	def invoke (self, elems, members):
		if not elems:
			raise gdb.GdbError("Empty elems")

		elem_type = elems.type.target()
		elems = array_of_pointers_to_list(elems);
		members = members.string().split()
		for elem in elems:
			print(elem)
			for member in members:
				value = None
				try:
					value = gdb.parse_and_eval(f'(({elem_type}){elem})->{member}')
				except:
					pass
				print(f' - {member}: {value}')
		# TODO this should better be a gdb.COMMAND rather than a function. We
		# return "" because a function must return something, although we don't
		# need to return anything.
		return ""

PrintElements()

class FilterElements(gdb.Function):
	""" Given an array of pointer, it filters them based on the provided filter expression.

$filter_elements(array, filter): For each element in "array", run the "filter" expression. If "filter" returns true, add the element into the returned set.
The "filter" argument is a gdb expression, where the special symbol {this} represents the current element in the array.
Examples:
	filter_elements($lx_threads(0), "{this}->pid > 3 && {this}->pid < 100")
"""
	def __init__(self):
		super (FilterElements, self).__init__("filter_elements")
	def invoke (self, elems, filterExpression):
		if not elems:
			raise gdb.GdbError("Empty elems")

		elem_type = elems.type.target()
		filterExpression = filterExpression.string()

		def userFilter(elem):
			log.debug(f'filtering elem {elem}')
			parsedFilter = filterExpression.replace('{this}', f'(({elem_type}){elem})')
			evalResult = gdb.parse_and_eval(parsedFilter)
			return evalResult == True

		elems = array_of_pointers_to_list(elems);
		filteredElements = list(filter(userFilter, elems))
		if not filteredElements:
			raise gdb.GdbError("The filtered list is empty")
		return ptr_list_to_value(filteredElements)

FilterElements()

class GroupByElements(gdb.Function):
	""" Given an array of pointer and a member, it classifies the items based on the member value.

$group_by_elements(array, member): For each element in "array", obtain the value of "member" and classify it. Then, it returns a list of elements by member and the count per group.
Examples:
	group_by_elements($lx_threads(0), "se.on_cpu")
"""
	def __init__(self):
		super (GroupByElements, self).__init__("group_by_elements")
	def invoke (self, elems, member):
		if not elems:
			raise gdb.GdbError("Empty elems")

		group = defaultdict(list)
		elem_type = elems.type.target()
		elems = array_of_pointers_to_list(elems);
		member = member.string()

		for elem in elems:
			log.debug(f'classifying element {elem}')
			value = gdb.parse_and_eval(f'(({elem_type}){elem})->{member}')
			group[str(value)].append(elem)

		total = 0
		for key, values in group.items():
			print(key)
			for value in values:
				print(f' - {value}')
				total += 1

		print("\n")
		print(f'Total: {total}')
		for key, values in group.items():
			print(f' - {key}: {len(values)}')

		return ""

GroupByElements()


# From https://stackoverflow.com/a/33212794/1725759
class NextInstructionAddress(gdb.Command):
    """
Run until Next Instruction address.

Usage: nia

Put a temporary breakpoint at the address of the next instruction, and continue.

Useful to step over int interrupts.

See also: http://stackoverflow.com/questions/24491516/how-to-step-over-interrupt-calls-when-debugging-a-bootloader-bios-with-gdb-and-q
"""
    def __init__(self):
        super().__init__(
            'nia',
            gdb.COMMAND_BREAKPOINTS,
            gdb.COMPLETE_NONE,
            False
        )
    def invoke(self, arg, from_tty):
        frame = gdb.selected_frame()
        arch = frame.architecture()
        pc = gdb.selected_frame().pc()
        length = arch.disassemble(pc)[0]['length']
        gdb.Breakpoint('*' + str(pc + length), temporary = True)
        gdb.execute('continue')

NextInstructionAddress()



class HelloWorld (gdb.Command):
	""" Greet the whole world. """

	def __init__(self):
		super (HelloWorld, self).__init__("hello-world", gdb.COMMAND_USER)
	def invoke (self, arg, from_tty):
		print("Hello World!")

HelloWorld()

logging.basicConfig(level=logging.INFO)
log.setLevel(logging.INFO)

# Uncomment the following line to enable debug logs
#log.setLevel(logging.DEBUG)

log.info("Custom python bindings loaded")
