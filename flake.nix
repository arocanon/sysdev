# flake.nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    #nixpkgs.follows = "nixdev/nixpkgs";

    nixdev.url  = "git+https://gitlab.bsc.es/arocanon/nixdev?ref=sched";
    nixdev.inputs.nixpkgs.follows = "nixpkgs";
    nixdev.inputs.bscpkgs.follows = "bscpkgs";

    eupilot.url = "git+ssh://git@gitlab-internal.bsc.es/arocanon/eupilot-fcs?ref=master&submodules=1";
    eupilot.inputs.nixpkgs.follows = "nixpkgs";
    eupilot.inputs.bscpkgs.follows = "bscpkgs";

    bscpkgs.url = "git+file:///mnt/data/kernel/fcs/bscpkgs";
  };

  outputs = { self, nixpkgs, nixdev, eupilot, ... }: let
    system = "x86_64-linux";
    nixdevOverlay = nixdev.overlays.default;
    eupilotOverlay = eupilot.overlays.default;
  in {
    nixosConfigurations.eda = nixpkgs.lib.nixosSystem {
      inherit system;
      modules = [
        ./system/eda/configuration.nix
      ];
      specialArgs = {
        inherit nixpkgs system;
        nixdevOverlay = (nixdevOverlay "none");
        eupilot = eupilot.legacyPackages.${system};
      };
    };

    # Linux Kernel development shell with menuconfig support
    devShells.${system}.default = self.nixosConfigurations.eda.config.boot.kernelPackages.kernel.overrideAttrs (old: {
      nativeBuildInputs = old.nativeBuildInputs ++ (with self.nixosConfigurations.eda.pkgs; [
        pkg-config ncurses
      ]);
    });

    tofuEmuDev = self.nixosConfigurations.tofu.config.boot.kernelPackages.kernel.overrideAttrs (old: {
      nativeBuildInputs = old.nativeBuildInputs ++ (with self.nixosConfigurations.tofu.pkgs; [
        pkg-config ncurses
      ]);
    });


    kernel = self.nixosConfigurations.eda.config.boot.kernelPackages.kernel;
    config = self.nixosConfigurations.eda.config.boot.kernelPackages.kernel.configfile;
    vmlinux = self.nixosConfigurations.eda.config.boot.kernelPackages.kernel.dev;
    pkgs = self.nixosConfigurations.eda.pkgs;

    # aarch64 cross
    kernelCross = self.nixosConfigurations.eda.pkgs.pkgsCross.aarch64-multiplatform.linuxPackages.kernel;
    vmlinuxCross = self.kernelCross.dev;
    configCross = self.kernelCross.configFile;
    pkgsCross = self.nixosConfigurations.eda.pkgs.pkgsCross.aarch64-multiplatform;


    nixosConfigurations.mn5 = nixpkgs.lib.nixosSystem {
      #inherit system;
      modules = [
        ./system/mn5/configuration.nix
      ];
      specialArgs = {
        inherit nixpkgs system;
        nixdevOverlay = (nixdevOverlay "sapphirerapids");
        eupilot = eupilot.legacyPackages.${system};
      };
    };

    nixosConfigurations.tofu = nixpkgs.lib.nixosSystem {
      system = "aarch64-linux";
      modules = [
        ./system/tofu/configuration.nix
      ];
      specialArgs = {
        inherit nixpkgs;
        system = "aarch64-linux";
        nixdevOverlay = (nixdevOverlay "native");
        eupilot = eupilot.legacyPackages."aarch64-linux";
      };
    };

    nixosConfigurations.milkv = nixpkgs.lib.nixosSystem {
      system = "riscv64-linux";
      modules = [
        ./system/milkv/configuration.nix
        ({
          nixpkgs.hostPlatform = {
            #gcc.arch = arch;
            #gcc.tune = arch;
            system = "riscv64-linux";
          };

          nixpkgs.buildPlatform = {
            gcc.arch = "native";
            gcc.tune = "native";
            system = "x86_64-linux";
          };
        })
      ];
      specialArgs = {
        inherit nixpkgs;
        system = "riscv64-linux";
        eupilotOverlay = (eupilotOverlay "none");
        eupilot = eupilot.legacyPackages."riscv64-linux";
      };
    };

    nixosConfigurations.potato = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ./system/milkv/configuration.nix
      ];
      specialArgs = {
        inherit nixpkgs;
        system = "x86_64-linux";
        eupilotOverlay = (eupilotOverlay "none");
        eupilot = eupilot.legacyPackages."x86_64-linux";
      };
    };

  };

  # nix develop prompt
  nixConfig.bash-prompt = "\\[\\e[1;32m\\][nix-develop]$\\[\\e[0m\\] ";
}
