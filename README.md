# NixOS VM

This repo features a simple NixOS with Flakes config, intended to develop the Linux Kernel.

Developing the Linux Kernel using the Nix sandbox is slow.
Every little change triggers a full clean rebuild.
In order to speed up builds, this project contains instructions on how to build an external kernel out-of-nix using make incremental builds and how to replace the NixOS kernel with the external kernel on a virtual machine (VM).
The main idea is to initially have the same kernel version in NixOS and the external kernel, and then modify the external kernel but keep the NixOS kernel as is.
Then, at boot time, replace the NixOS kernel with the external Kernel using qemu.
This implies that, on the VM, we use the external kernel but the NixOS kernel modules (e.g. `virtio_balloon`).
If you want do develop Linux kernel modules instead of the core kernel, an option is to build your module within the core kernel (option 'y' instead of 'm' in the kernel config).

To get started with this repo:

```sh
mkdir sysdev
cd sysdev
git clone git@gitlab-internal.bsc.es:arocanon/sysdev.git ./vm
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git ./src
```

Througout this guide, we assume that the default target machine is "eda" (`x86_64`) with vm support.

## Reference

 - [NixOS wiki on the Linux Kernel](https://nixos.wiki/wiki/Linux_kernel)
 - [NixOS with flakes on VM gist](https://gist.github.com/FlakM/0535b8aa7efec56906c5ab5e32580adf)
 - [NixOS VM with manual kernel build](https://alberand.com/nixos-linux-kernel-vm.html)
 - [NixOS wiki on Linux Kernel Debugging with GDB](https://nixos.wiki/wiki/Kernel_Debugging_with_QEMU)
 - [Kernel kgdb and kdb setup](https://www.kernel.org/doc/html/latest/dev-tools/kgdb.html)
 - [Kernel gdb ptyhon scripts](https://www.kernel.org/doc/html/latest/dev-tools/gdb-kernel-debugging.html)

## VM Config

You can tweak the VM config on this repo `system/eda/configuration.nix`:

```nix
  virtualisation.vmVariant = {
  ...
  }
```

## Build NixOS VM

```sh
$ nixos-rebuild build-vm --flake .#nixos
```
## Boot the VM

The main script to boot the vm is `bin/boot.sh`. Please, see `bin/boot.sh --help` for a list of options.
In the rest of this README, you can find usage scenarios for the most common options.
To kill them vm, just run `pkill qemu`.

### Login

Both user and password are `admin` by default. However, admin logs in automatically and sudo does not require a password.

### Using ssh

Boot vm with an open ssh port:

```sh
$ bin/boot.sh -n
```
Then connect with

```sh
$ `bin/ssh.sh
```

### (old) Using serial connection

By default, running the vm starts a serial session without graphics.

```sh
$ ./result/bin/run-*-vm
```

Apparently, older versions needed to set it explicitly:

```sh
$ QEMU_KERNEL_PARAMS=console=ttyS0 ./result/bin/run-*-vm -nographic
```

## Kernel logs

The default loglevel is set in `system/kernel/default.nix`

```nix
  boot.consoleLogLevel = 7;
```

But it can be overriden at runtime:

```sh
$ bin/boot.sh -l 7
```

## External Kernel setup

> NOTE: This will use NixOS shipped kernel modules, not your manually built modules!

There are two kernels. The NixOS kernel, and the kernel you manually build
outside NixOS for development. You need to build the same kernel in both NixOS
and outside NixOS, since we will use the core kernel build manually but the
modules of the NixOS kernel. Therefore, the versions and config need to match.

### Build the VM
 * Set your kernel source in the NixOS config under `system/kernel/default.nix`. You will need to set you kernel version, git url and commit.
 * Build the VM as explained [here](#build-nixos-vm)

### Build the external Kernel
The first time you build your kernel, you can run `bin/clean_build.sh` in a nix develop shell. After the first build, you can run `bin/rebuild.sh` to rebuild incrementally.

The do a clean build manually instead of using the provided scripts, follow these steps:
 * Generate the NixOS kernel config
   ```sh
   $ nix build .#nixosConfigurations.eda.config.boot.kernelPackages.kernel.configfile
   ```
   or use the alias
   ```sh
   $ nix build .#config
   ```
 * Copy the config to the build dir
   ```sh
   $ mkdir ../build
   $ cp ./result ../build/.config
   ```
 * Get a kernel build shell (no menuconfig)
   ```sh
   $ nix develop .#nixosConfigurations.eda.config.boot.kernelPackages.kernel
   ```
   or simply (this includes the dependencies needed to use menuconfig)
   ```sh
   $ nix develop
   ```
 * Build an uncompressed kernel manually. This build only the core kernel, not the modules.
   ```sh
   $ make O=../build LOCALVERSION= vmlinux -j$(nproc)
   ```
   > Note: You need to build the kernel with `CONFIG_PVH` for qemu to be able to boot an uncompressed image. This should be set by default in NixOS.

### Boot NixOS with external kernel
 * Launch the vm with the manually compiled kernel with:
   ```sh
   $ bin/boot.sh -u
   ```

### (optional) Build compressed image

If you want to build a compressed image (bzImage), follow these steps:

```sh
$ make O=../build LOCALVERSION= bzImage -j$(nproc)
```
And boot with

```sh
 $ bin/boot.sh -c
```

## Kernel debugging with GDB

We can debug the kernel either with gdb with native qemu support or with Linux KGDB.
Qemu support is faster and simpler, but it lacks linux-specific knowledge (e.g. running "bt" gdb command will show cpus, but not tasks).
KGDB is slower, but is linux-aware (KGDB is a Linux built-in).
When debugging a VM, I would use qemu support.
When debugging a kernel running in a remote machine, I would use KGDB.

In both cases, we can make use of the Linux Python scripts for gdb for debugging (We need to enable a Linux config for these scripts to be built).
This project distributes tools to make it easier to debug the kernel with python scripts.

### Qemu gdb support

To debug the kernel using the qemu's gdbserver, simply boot the machine with:

```sh
 $ bin/boot.sh -g [-u]`
```
Then, attach gdb to it with

```sh
 $ bin/gdb.sh
```
You can now press "c" to continue running the kernel, and you can stop the execution by pressing ctrl-c.

### KGDB

Enable the `kgdb` param of the function `nixos-kernel`.
```sh
vi system/kernel/default.nix
```
Then, rebuild both the NixOS VM and the external kernel using the new nixos config.

Once built, boot the kernel with a virtual serial device redirected to a tcp port for gdb to connect to.
Note that because the serial output is being redirected to a socket, so you won't see boot logs on the terminal.
Therefore, you should use `-n` to enable networking and connect with ssh.

```sh
 $ bin/boot.sh -k -n [-u]`
```
Then, ssh into the kernel with:

```sh
 $ bin/ssh.sh
```

Once logged in, setup kgdb with:

```sh
 (eda)$ bin/kgdb-setup.sh
```
At this point, the virtual serial device no longer outputs logs, but it waits for a gdb connection.
Then, connect with gdb on the host:

```sh
 $ bin/gdb.sh
```

Note that on gdb/KGDB you cannot stop the execution with ctrl-c.
To trigger a gdb break, you need to force it with a sysrq. 
The script `bin/kgdb-setup.sh` on the vm does this for you.

### GDB with python bindings

Linux gdb scripts are automatically loaded when executing `bin/gdb.sh`.
Once inside gdb, you can run some new commands:

```gdb
(gdb) apropos lx # show list of main commands and functions with a short description (there are more, check the source)
(gdb) help lx-ps # show help for command lx-ps
(gdb) lx-ps # run command to show tasks
(gdb) p $lx_current().pid # print pid of "current" task (note this is a function, not a command)
  ```
(see more info [here](https://www.kernel.org/doc/html/latest/dev-tools/gdb-kernel-debugging.html))

### Using KGDB

kgdb works just as gdb, but kgdb does not show threads when running `i thr` but cpus (and the thread they are running on).

By default, when you step with `s`, only the current thread steps, and all the others can continue running freely until the current thread ends stepping.
Also, this can lead to other threads "seizing the prompt". To really step a single cpu at a time, use:
```gdb
set scheduler-locking step
```
To return to the previous behavior:
```gdb
set scheduler-locking off
```
More info on [scheduler-locking](https://sourceware.org/gdb/current/onlinedocs/gdb.html/All_002dStop-Mode.html) and [thread managing](https://sourceware.org/gdb/current/onlinedocs/gdb.html/Thread-Stops.html#Thread-Stops).

To set a breakpoint on a syscall you need the full syscall name. Search for it using

```gdb
(gdb) info function clock_nanosleep
All functions matching regular expression "clock_nanosleep":

File /mnt/data/kernel/fcs/src/kernel/time/posix-timers.c:
1373:   long __ia32_sys_clock_nanosleep(const struct pt_regs *);
1401:   long __ia32_sys_clock_nanosleep_time32(const struct pt_regs *);
1373:   long __x64_sys_clock_nanosleep(const struct pt_regs *);
1401:   long __x64_sys_clock_nanosleep_time32(const struct pt_regs *);
```
Not all commands provided by the linux python bindings are shown with `(gdb) apropos lx`.
To see all comands, check the linux source `scripts/gdb/linux`.

Read per-cpu data (e.g. `cpu_rq(smp_processor_id())->cfs.nr_running`).

```gdb
(gdb) $lx_per_cpu("runqueues").cfs.nr_running
```

Set memory watchpoint on address (note, even if `nr_running` is an int, no & is needed here, `-l` automatically computes the addres from the expression)

```gdb
(gdb) watch -l $lx_per_cpu("runqueues").cfs.nr_running
```

Use gdb variables

```gdb
(gdb) set $cfs_rq = & $lx_per_cpu("runqueues").cfs
(gdb) p $cfs_rq->nr_running
or
(gdb) set $cfs_rq = $lx_per_cpu("runqueues").cfs
(gdb) p $cfs_rq.nr_running
```

Use `container_of`

```gdb
(gdb) set $cfs_rq = & $lx_per_cpu("runqueues").cfs
(gdb) set $se = $cfs_rq->curr
(gdb) set $p = $container_of($se, "struct task_struct", "se")
(gdb) p $p->pid
```

Create custom gdb commands (see [more](https://sourceware.org/gdb/current/onlinedocs/gdb.html/Define.html#Define)). Write inside `.gdbinit` in the root of this repo. Note that we cannot return a value direclty.
Only print an output to the console or set global variables.

```gdb
define cfs_rq
    print & $lx_per_cpu("runqueues").cfs
end
```

Create complex gdb commands using python bindings (see [all doc](https://sourceware.org/gdb/current/onlinedocs/gdb.html/Python.html#Python), [functions](https://sourceware.org/gdb/current/onlinedocs/gdb.html/Functions-In-Python.html#Functions-In-Python), [Commands](https://sourceware.org/gdb/current/onlinedocs/gdb.html/CLI-Commands-In-Python.html#CLI-Commands-In-Python), [types](https://sourceware.org/gdb/current/onlinedocs/gdb.html/Types-In-Python.html), [values](https://sourceware.org/gdb/current/onlinedocs/gdb.html/Values-From-Inferior.html#Values-From-Inferior)).
See `./gdbcmds.py` HelloWorld class example. `gdbcmds.py` is loaded by `bin/kgdb.sh` on startup.

```gdb
(gdb) hello-world
```

To debug your gdb python script, note that you can reload the python script within the gdb session:

```gdb
(gdb) source ./gdbcmds.py
```

In case of trouble, you can enable debug logs of the `gdbcmds.py` file by uncommenting the following line at the end of the file.

```python3
#log.setLevel(logging.DEBUG)
```

### Native NixOS vmlinux

If you want debug the native NixOS kernel with kgdb, you need the NixOS kernel symbols.
```sh
nix build .#eda.config.boot.kernelPackages.kernel.dev;
```
or use the alias
```sh
nix build .#vmlinux
```
The final vmlinux can be found under `result-dev/vmlinux`.


## Storage

The vm creates a disk in the current directory named `$(hostname).qcow2` after launch. You can delete it for a fresh start, if needed.

## Running bare-metal

The tofu system can run in a AWS machine.

### Installation

 - Start an aws arm64 instance using a [pre-generated NixOS AMI](https://nixos.github.io/amis/) (or check [here](https://nixos.wiki/wiki/Install_NixOS_on_Amazon_EC2)).
 - Wait until the machine has booted, get the `<ip>` and Connect to the instance using ssh and the aws provided key:
   ```sh
   host$ ssh-add .ssh/id_ed25519 # or/and .ssh/id_rsa # load host keys into ssh agent
   host$ ssh -A -i ./aws-key.pem root@<ip> # connect and forward keys to remote machine
   ```
 - Install basic tools in the remote machine:
   ```sh
   remote# nix --extra-experimental-features nix-command --extra-experimental-features flakes profile install nixpkgs#vim nixpkgs#git nixpkgs#tmux nixpkgs#openfortivpn
   ```
 - Clone this repo
   ```sh
   remote# git clone git@gitlab-internal.bsc.es:arocanon/sysdev.git
   remote# cd sysdev
   ```
 - Install system
   ```sh
   remote# nixos-rebuild switch --flake .#tofu
   remote# reboot
   ```
 - Login with new credentials
   ```sh
   remote# ssh -A admin@<ip>
   ```

### ssh agent forward credentials

Forward the ssh keys on your host machine to the remote machine. This allows you to use you private ssh keys as if they were stored in the remote machine (even if they are not).

```sh
host$ ssh-add .ssh/id_ed25519 # or/and .ssh/id_rsa
host$ ssh -A admin@<ip>
```

In the remote machine, it sets the `SSH_AUTH_SOCK` env with a path to an ssh socket. Every time you create a new ssh connection, this socket changes. Note that if you use tmux and reattach to an existing session, your `SSH_AUTH_SOCK` env will be stale. To reload them, run the `fixssh` bash command inside tmux. Also note that new tmux windows/panes will already have the new env.

```sh
# inside tmux
$ fixssh
```

**TODO** possibly use a [fixed named socket](https://stackoverflow.com/a/23187030/1725759) to fix this.

# Kernel Debug Guide

 - [Softlockup and hardlockup detector](https://docs.kernel.org/admin-guide/lockup-watchdogs.html)
 - [Lockdep](https://lwn.net/Articles/185666/)
 - [Lockstat](https://docs.kernel.org/locking/lockstat.html)

## Kernel lock contention

There are several ways to obtain metrics on internal kernel lock contentions.

The most efficient and precise option is [lockstat](https://docs.kernel.org/locking/lockstat.html), but it requries to build the kernel with `CONFIG_LOCK_STAT`.
Interally, the kernel keeps in-memory counts of lock statistics which are exported to `/proc/lock_stat` if tracing is enabled through `/proc/sys/kernel/lock_stat/`.

Another option is using `perf` to record tracepoints about events and analize them offline.
This, approach is not suitable for intense workload that produce lots of events, because the result trace might be too big to analize and the overhead of writting it might affect performance.
However, it allows to inspect events individually.
Example:

```sh
 $ perf lock record sleep 1
 $ perf lock contention
```

It is also possible to use `perf` with bpf to avoid writting the whole trace. However, I have not been able to make it work for now.

```sh
 $ perf lock record -b sleep 1
 $ perf lock contention
```

LTTng also has support for lock contention events, which can be later analized on Trace Compass.
However, the LTTng code around this tracepoints is not built-in by default.
This repo contains patches to enable these events.

# FAQ

## GDB does not find linux symbols
Are you booting your kernel with `nokaslr`?
