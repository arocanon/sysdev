#!/usr/bin/env bash

dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
blddir=$dir/../../build
srcdir=$dir/../../src

rm -rf $blddir
mkdir $blddir

nix build .#nixosConfigurations.tofu.config.boot.kernelPackages.kernel.configfile;
cp ./result $blddir/.config

cd $srcdir
#make O=$blddir LOCALVERSION= ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- menuconfig
make O=$blddir LOCALVERSION= ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- vmlinux -j14
make O=$blddir LOCALVERSION= ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- scripts_gdb -j14
