#!/usr/bin/env bash

dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
blddir=$dir/../../build
srcdir=$dir/../../src

cd $srcdir
make O=$blddir LOCALVERSION= vmlinux -j14
