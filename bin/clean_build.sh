#!/usr/bin/env bash

dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
blddir=$dir/../../build
srcdir=$dir/../../src

rm -rf $blddir
mkdir $blddir

nix build .#config
cp ./result $blddir/.config

cd $srcdir
make O=$blddir LOCALVERSION= vmlinux -j14
make O=$blddir LOCALVERSION= scripts_gdb -j14
