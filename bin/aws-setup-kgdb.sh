#!/usr/bin/env bash

inst=i-03b80ce024ec50710
region=us-east-1
endpoint=""


case $region in
us-east-1)
	endpoint=serial-console.ec2-instance-connect.us-east-1.aws
	;;
*)
	echo "No fingerprint available for region $region"
	echo "Please, check https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/connect-to-serial-console.html#sc-endpoints-and-fingerprints"
	exit 1
esac

# Install public ssh key on aws serial endpoint
aws ec2-instance-connect send-serial-console-ssh-public-key \
    --instance-id $inst \
    --serial-port 0 \
    --ssh-public-key file://$HOME/.ssh/id_rsa.pub \
    --region $region

# Create a socket that forwards the aws serial console over ssh
socat tcp-listen:1234,reuseaddr,fork exec:"ssh -tt $inst.port0@$endpoint"
