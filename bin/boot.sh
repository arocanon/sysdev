#!/usr/bin/env bash

dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
blddir=`readlink -f $dir/../../build`


function usage() {
	echo "Usage: $(basename $0) [OPTIONS]"
	echo "Where OPTIONS are:"
	echo " -e | --external <kernel_path>: Boot external kernel in <kernel_path>"
	echo " -u | --uncompressed: Boot uncompressed external kernel in default path ($opt_compressed)"
	echo " -c | --compressed: Boot compressed external kernel in default path ($opt_uncompressed)"
	echo " -n | --network: Boot with networking (default is boot without networking)"
	echo " -l | --loglevel <loglevel>: Boot with <loglevel>"
	echo " -d | --dev: Development mode, implies -n, -u, -k, -l7"
	echo " -k | --kgdb: Boot with kernel kgdb support, implies \"--serial\" and kernel boot option \"nokaslr\""
	echo " -g | --gdb: Boot with qemu gdb support, implies kernel boot option \"nokaslr\""
	echo " -s | --stopped: Start stopped and waits for gdb connection (implies -k)"
	echo " -v | --verbose: Print command and env"
	echo " -y | --dry-run: Do all but starting the vm. Implies -v"
	echo " -y | --dry-run: Do all but starting the vm. Implies -v"
	echo " -t | --boot-tracepoints: Enable boot ftrace tracepoints"
	echo " -i | --serial: Enable serial port /dev/ttyUSB0 on host"
	exit 1
}


opt_external=0
opt_uncompressed=$blddir/vmlinux
opt_compressed=$blddir/arch/x86_64/boot/bzImage
opt_kpath=""
opt_logs=""
opt_kgdb=0
opt_gdb=0
opt_stopped=0
opt_network=0
opt_verbose=0
opt_dryrun=0
opt_boottracepoints=0
opt_serial=0


VALID_ARGS=$(getopt -o hncue:l:kgdsvyti --long help,network,compressed,uncompressed,external:,loglevel:,kgdb,gdb,dev,stopped,verbose,dry-run,boot-tracepoints,serial -- "$@")
if [[ $? -ne 0 ]]; then
	exit 1
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
	case "$1" in
		-h | --help)
			usage
			shift
			;;
		-n | --network)
			opt_network=1
			shift
			;;
		-c | --compressed)
			opt_external=1
			opt_kpath=$opt_compressed
			shift
			;;
		-u | --uncompressed)
			opt_external=1
			opt_kpath=$opt_uncompressed
			shift
			;;
		-e | --external)
			opt_external=1
			opt_kpath="$2"
			shift 2
			;;
		-l | --loglevel)
			opt_logs="$1"
			shift 2
			;;
		-k | --kgdb)
			opt_kgdb=1
			opt_serial=1
			shift
			;;
		-g | --gdb)
			opt_gdb=1
			shift
			;;
		-d | --dev)
			opt_gdb=1
			opt_network=1
			opt_external=1
			opt_kpath=$opt_uncompressed
			opt_logs=7
			shift
			;;
		-s | --stopped)
			opt_gdb=1
			opt_stopped=1
			shift
			;;
		-v | --verbose)
			opt_verbose=1
			shift
			;;
		-y | --dry-run)
			opt_verbose=1
			opt_dryrun=1
			shift
			;;
		-t | --boot-tracepoints)
			opt_boottracepoints=1
			shift
			;;
		-i | --serial)
			opt_serial=1
			shift
			;;
		--)
			shift
			break
			;;
		*)
			usage
			;;
	esac
done

nixpkgs_qemu_kernel=""
qemu_net_opts=""
qemu_kernel_params=""
qemu_args=""
qemu_opts=""
env=""

if [ $opt_external -eq 1 ]; then
	if [[ ! -r "$opt_kpath" ]]; then
		echo "Error: could not find external kernel vmlinux in $opt_kpath"
		exit 1
	fi

	# Set kernel path for qemu
	nixpkgs_qemu_kernel=$opt_kpath
fi

if [ $opt_network -eq 1 ]; then
	qemu_net_opts="hostfwd=tcp::2221-:22"
fi

if [ -n "$opt_logs" ]; then
	qemu_kernel_params="loglevel=7 $qemu_kernel_params"
fi

if [ "$opt_kgdb" -eq 1 ]; then
	qemu_kernel_params="kgdboc=ttyS0,115200 nokaslr $qemu_kernel_params"
fi

if [ "$opt_gdb" -eq 1 ]; then
	qemu_kernel_params="nokaslr $qemu_kernel_params"
	# Shorthand for -gdb tcp::1234, i.e. open a gdbserver on TCP port 1234
	qemu_args="$qemu_args -s"
fi

if [ $opt_stopped -eq 1 ]; then
	# Do not start CPU at startup (you must type 'c' in the monitor).
	qemu_args="$qemu_args -S"
fi

if [ $opt_boottracepoints -eq 1 ]; then
	qemu_kernel_params="trace_options=sym-addr trace_event=sched:* tp_printk trace_buf_size=1M loglevel=7 $qemu_kernel_params"
fi

if [ $opt_serial -eq 1 ]; then
	echo "Serial output is being redirected to tcp:localhost:1234. The boot longs won't be displayed here."
	qemu_opts="$qemu_opts -serial mon:tcp:localhost:1234,server=on,wait=off"
fi

env+="NIXPKGS_QEMU_KERNEL_eda=\"$nixpkgs_qemu_kernel\" "
env+="QEMU_NET_OPTS=\"$qemu_net_opts\" "
env+="QEMU_KERNEL_PARAMS=\"$qemu_kernel_params\" "
env+="QEMU_OPTS=\"$qemu_opts\" "

if [ $opt_verbose -eq 1 ]; then
	echo "$env ./result/bin/run-*-vm $qemu_args"
fi

if [ $opt_dryrun -eq 1 ]; then
	exit 0
fi

eval "$env" ./result/bin/run-*-vm $qemu_args
