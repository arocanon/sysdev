#!/usr/bin/env bash

dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
nixosdir=$dir/..
blddir=$nixosdir/../build
defaultkpath=`readlink -f $blddir/vmlinux`

kpath=${1:-$defaultkpath}
vmlinuxgdb=`readlink -f $blddir/vmlinux-gdb.py`

if [[ ! -r $kpath ]]; then
	echo "Error: could not find external kernel vmlinux in \"$kpath\""
	exit 1
fi

if [[ ! -r $vmlinuxgdb ]]; then
	echo "Error: could not find gdb scripts in \"$vmlinuxgdb\""
	exit 1
fi

gdb \
	-ex "target remote :1234" \
	-iex "add-auto-load-safe-path $nixosdir" \
	-iex "add-auto-load-safe-path $vmlinuxgdb" \
	-ex "source $nixosdir/gdbcmds.py" \
	$kpath



