#!/usr/bin/env bash

workload=0
cores=96
timeout=30
pool=cycle
version=ult
nthreads=98304

for rep in `seq 1 10`; do
	log=$(mktemp /tmp/nex-log.XXXXXX)
	rclog=$(mktemp /tmp/nex-log-rc.XXXXXX)
	{
		rm -f /dev/shm/nosv-sc;


		timeout -s KILL 2m ./bin/util-linux/bin/taskset -c 0-$(( cores - 1 )) ./bin/time/bin/time -f "ictx:%c\nvctx:%w" ./bin/pipe/bin/fcs-pipe -t $timeout -c $workload -n $nthreads -p $pool -m $version
		echo $? > $rclog

	} 2>&1 | tee $log

	retcode=$(cat $rclog)
	echo "return code for repetition $rep: $retcode"
	awk '
	  /time:/ {print "time:"$2}
	  /Iters\/s:/ {print "lps:"$2}
	' $log;
	tail -n 3 $log;

	echo ------------------------------
	rm $rclog
	rm $log
done
