#!/usr/bin/env python3

import pandas as pd
import seaborn as sns
import os

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
import seaborn.objects as so
import numpy as np
from decimal import Decimal

cycles_per_loop = 100
order = ["pthread", "fcs", "fcs-bypass", "ult", "coro", "nop"];



def plot_time_distribution2(df, params, metrics):
  global df_norm

  order = ["pthread", "fcs", "fcs-bypass", "ult", "coro"];

  df = df.copy()

  df = df[df.version != "nop"]
  df = df.rename(columns={"version": "Ctx Time"})
  df["workload"] = df["workload"] * cycles_per_loop
  df["ymin"] = df["per_ctx"] - df["per_ctx"]
  df["ymax"] = df["per_ctx"] + df["per_ctx"]
  df.workload = df.workload.astype('str')

  df_norm = df

  df['per_ctx'] = df['per_ctx'].astype('float64')
  df['per_rt']  = df['per_rt'].astype('float64')
  df['per_wl']  = df['per_wl'].astype('float64')

  df_rt = df.copy()
  df_rt["per_ctx"] = df_rt["per_rt"] + df_rt["per_ctx"]

  df_wl = df.copy()
  df_wl["per_ctx"] = df_wl["per_rt"] + df_wl["per_ctx"] + df_wl["per_wl"]

  (
    so.Plot(df, x = "workload", y = "per_ctx", color = "Ctx Time")
    .add(so.Bar(alpha=0.2, color="teal",   edgewidth=0.5), so.Agg(), so.Dodge(), data=df_wl, label = "Workload")
    .add(so.Range(color="green"), so.Est(errorbar="se"), so.Dodge(), legend = False, data=df_wl)
    .add(so.Bar(alpha=0.2, color="yellow", edgewidth=0.5), so.Agg(), so.Dodge(), data=df_rt, label = "Runtime")
    .add(so.Bar(alpha=0.8), so.Agg(), so.Dodge())
    .add(so.Range(color="black"), so.Est(errorbar="se"), so.Dodge(), legend = False, data = df) # Not working :(
    .scale(color=so.Nominal(order=order))
    .label(x="Workload (cycles per loop)", y="Time distribution (%)", legend="App Time")
  ).save("holi.pdf", bbox_inches="tight")

def plot_time_distribution(df, params, metrics):
  global df_norm

  order = ["pthread", "fcs", "fcs-bypass", "ult", "coro"];

  df = df.copy()

  df = df.groupby(params)[metrics].agg(['mean', 'std']).reset_index()
  df.columns = ['_'.join(col) if (col[0] != "" and col[1] != "") else col[0] for col in df.columns]

  df = df[df.version != "nop"]
  df = df.rename(columns={"version": "Ctx Time"})
  df["workload"] = df["workload"] * cycles_per_loop
  df["ymin"] = df["per_ctx_mean"] - df["per_ctx_std"]
  df["ymax"] = df["per_ctx_mean"] + df["per_ctx_std"]
  df.workload = df.workload.astype('str')

  df_norm = df

  # we need to convert data types to make seaborn happy
  df['per_ctx_mean'] = df['per_ctx_mean'].astype('float64')
  df['per_ctx_std']  = df['per_ctx_std'].astype('float64')
  df['per_rt_mean']  = df['per_rt_mean'].astype('float64')
  df['per_rt_std']   = df['per_rt_std'].astype('float64')
  df['per_wl_mean']  = df['per_wl_mean'].astype('float64')
  df['per_wl_std']   = df['per_wl_std'].astype('float64')

  # calculate std ranged manually
  df['ymin'] = df['per_ctx_mean'] - df['per_ctx_std']
  df['ymax'] = df['per_ctx_mean'] + df['per_ctx_std']

  # Create a copy to plot the rt values overriding the default ones
  df_rt = df.copy()
  df_rt["per_ctx_mean"] = df_rt["per_rt_mean"] + df_rt["per_ctx_mean"]
  df_rt['ymin'] = df_rt['per_ctx_mean'] - df_rt['per_rt_std']
  df_rt['ymax'] = df_rt['per_ctx_mean'] + df_rt['per_rt_std']

  # Create a copy to plot the wl values overriding the default ones
  df_wl = df.copy()
  df_wl["per_ctx_mean"] = df_wl["per_rt_mean"] + df_wl["per_ctx_mean"] + df_wl["per_wl_mean"]
  df_wl['ymin'] = df_wl['per_ctx_mean'] - df_wl['per_wl_std']
  df_wl['ymax'] = df_wl['per_ctx_mean'] + df_wl['per_wl_std']

  (
    so.Plot(df, x = "workload", y = "per_ctx_mean", color = "Ctx Time", ymin = "ymin", ymax = "ymax")
    .add(so.Bar(alpha=0.2, color="teal",   edgewidth=0.5), so.Dodge(), data=df_wl, label = "Workload")
    .add(so.Bar(alpha=0.2, color="yellow", edgewidth=0.5), so.Dodge(), data=df_rt, label = "Runtime")
    .add(so.Bar(alpha=0.8), so.Dodge())
    .add(so.Range(color = "black"), so.Dodge(), data = df, legend = False)
    .add(so.Range(color = "goldenrod"), so.Dodge(), data = df_rt, legend = False)
    .add(so.Range(color = "darkgreen"), so.Dodge(), data = df_wl, legend = False)
    .scale(color=so.Nominal(order=order))
    .label(x="Workload (cycles per loop)", y="Time distribution (%)", legend="App Time")
  ).save("time_dist.pdf", bbox_inches="tight")

def gridplot_workload_normalized2(df):
  df = df.copy()
  df.columns = ['_'.join(col) if (col[0] != "" and col[1] != "") else col[0] for col in df.columns]
  order = ["pthread", "fcs", "fcs-bypass", "ult", "coro"];

  global df_norm

  df = df[df.version != "nop"]
  df = df.rename(columns={"version": "Ctx Time"})
  df["workload"] = df["workload"] * cycles_per_loop
  df["sp_mean"] = 100 - df["sp_mean"]
  df["ymin"] = df["sp_mean"] - df["sp_std"]
  df["ymax"] = df["sp_mean"] + df["sp_std"]
  df.workload = df.workload.astype('str')

  df_norm = df

  df_total = df.copy()
  df_total["sp_mean"] = 100

  df_rt = df.copy()
  df_rt["sp_mean"] = df_rt["rt_mean"] + df_rt["sp_mean"]

  (
    so.Plot(df, x = "workload", y = "sp_mean", color = "Ctx Time", ymin = "ymin", ymax = "ymax")
    .add(so.Bar(alpha=0.2, color="teal", edgewidth=0.5), so.Dodge(), data=df_total, label = "Useful")
    .add(so.Bar(alpha=0.2, color="yellow", edgewidth=0.5), so.Dodge(), data=df_rt, label = "Runtime")
    .add(so.Bar(alpha=0.8), so.Dodge())
    .add(so.Range(color="black"), so.Dodge(), legend=False)
    .scale(color=so.Nominal(order=order))
    .label(x="Workload (cycles per loop)", y="Time distribution (%)", legend="App Time")
  ).save("holi.png", bbox_inches="tight")


def gridplot_workload_normalized4(df):
  df = df.copy()
  df["workload"] = df["workload"] * cycles_per_loop
  df["ctxlat"] = 100

  g = sns.barplot(data = df, x = "workload", y = "ctxlat", hue = "version",
    hue_order = order, alpha = .4, legend = False
  )
  g = sns.barplot(data = df, x = "workload", y = ("sp", "mean"), hue = "version",
    hue_order = order, alpha = 1
  )
  sns.move_legend(g, "lower right", ncol = 1, title = None,  bbox_to_anchor=(1, 0))

  g.set(xlabel="Workload (cycles per loop)", ylabel="Loops per Second (%)")
  g.get_figure().suptitle('Performance of pipe benchmark')
  g.get_figure().savefig(f"pipe-wl2.png")
  g.get_figure().clf()

def gridplot_workload_normalized3(df):
  df = df.copy()
  df.set_index("version", inplace=True)
  df["workload"] = df["workload"] * cycles_per_loop
  df["ctxlat"] = 100 - df["sp"]["mean"]

  fig, ax = plt.subplots()
  bars_total = ax.bar(df.index, df['sp']['mean'], yerr = df['sp']['std'], label = "Total", color = 'blue')

  ax.set_xlabel("Workload (cycles per loop)")
  ax.set_ylabel("Normalized Loops per Second (%)")
  ax.set_title('Performance of pipe benchmark')
  ax.legend()
  fig.savefig('pipe-wl2.png')

def gridplot_workload_normalized(df):
  df = df.copy()
  df["workload"] = df["workload"] * cycles_per_loop

  g = sns.catplot(
    data = df, kind = "bar",
    x = "workload", y = "sp", hue = "version", col = "pool", errorbar = "sd",
    sharey = True,
    hue_order = order, alpha = .6, height = 5
  )
  g.set(xlabel="Workload (cycles per loop)", ylabel="Normalized Loops per Second (%)")
  #sns.move_legend(g, "upper center", ncol = 6, title = None,  bbox_to_anchor=(.55, .45), frameon=False)
  g.fig.suptitle('Performance of pipe benchmark')
  g.fig.savefig(f"pipe-wl.png")
  g.fig.clf()

def gridplot_workload(df):
  x = df["workload"] * cycles_per_loop

  g = sns.barplot(
    x = x, y = df["lps"], hue = df["version"], errorbar = "sd",
    hue_order = order, alpha = .6
  )
  g.set_yscale("log")
  g.set(xlabel="Workload (cycles per loop)", ylabel="Loops per Second")
  #sns.move_legend(g, "upper center", ncol = 6, title = None,  bbox_to_anchor=(.55, .45), frameon=False)
  g.get_figure().suptitle('Performance of pipe benchmark')
  g.get_figure().savefig(f"pipe-wl.png")
  g.get_figure().clf()

def gridplot_cores(df):
  g = sns.catplot(
    data = df, kind = "bar",
    x = "cores", y = "lps", hue = "version", col = "workload", errorbar = "sd",
    sharey = False,
    hue_order = order, alpha = .6, height = 5, col_wrap = 3
  )
  g.set(xlabel="Core Count", ylabel="Loops per Second")
  #sns.move_legend(g, "upper center", ncol = 6, title = None,  bbox_to_anchor=(.55, .45), frameon=False)
  g.fig.subplots_adjust(top=0.9) # adjust the Figure in rp
  g.fig.suptitle('Performance of pipe benchmark')
  g.fig.savefig(f"pipe.png")
  g.fig.clf()

def plot_sp_ctx_single(df, workload, ax1, ax2):
  df = df[df["workload"] == workload].copy()
  df.workload = df.workload.astype('str')
  df.cores = df.cores.astype('str')

  # plot the total number of context switches
  sns.barplot(data = df, x = "cores", y = "tctx", hue = "version",
    hue_order = order,
    palette = ["violet", "limegreen", "bisque", "aquamarine", "lightcoral", "lightgrey"],
    ax = ax1,
  )
  # plot the total number of involuntary context switches
  sns.barplot(data = df, x = "cores", y = "ictx", hue = "version",
    hue_order = order,
    palette = ["darkviolet", "forestgreen", "darkorange", "dodgerblue", "darkred", "darkgrey"],
    ax = ax1,
  )
  ax1.set_yscale("log")
  # plot the speedup
  sns.lineplot(data = df[df["version"] != "pthread"], x = "cores", y = "sp", hue = "version",
    hue_order = order[1:],
    palette = ["yellowgreen", "orange", "blue", "red", "grey"],
    ax = ax2,
  )
  ax1.set(title=f"workload = {workload}")
  # remove automatic legend
  ax1.legend([],[], frameon=False)
  ax2.legend([],[], frameon=False)

def plot_speedup_ctx(df, workload):
  fig, axes = plt.subplots(1, len(workload), sharex = True, sharey=True, layout="constrained")

  if len(workload) == 1:
    axes = [ axes ]

  paxis = []
  saxis = []
  for ax1 in axes:
    ax2 = ax1.twinx()
    paxis.append(ax1)
    saxis.append(ax2)

  # share secondary axes
  for ax in saxis[1:]:
    ax.sharey(saxis[0])

  # plot data
  for ax1, ax2, wl in zip(paxis, saxis, workload):
    plot_sp_ctx_single(df, wl, ax1, ax2)

  # hide secondary axes ticks but for the last one
  for ax2 in saxis[:-1]:
    for tk in ax2.get_yticklabels():
      tk.set_visible(False)

  # hide individual x and y axis title
  for ax1, ax2 in zip(paxis, saxis):
    ax1.set(xlabel="", ylabel="")
    ax2.set(xlabel="", ylabel="")

  # manually set legend
  fig.legend(title = "Context-switches", loc='center', bbox_to_anchor=(0.5, -0.1), ncol = 3,
    handles=[
      mpatches.Patch(color='violet',      label='pthread total'),
      mpatches.Patch(color='darkviolet',  label='pthread involuntary'),
      mpatches.Patch(color='limegreen',   label='fcs total'),
      mpatches.Patch(color='forestgreen', label='fcs involuntary'),
      mpatches.Patch(color='bisque',      label='fcs bypass total'),
      mpatches.Patch(color='darkorange' , label='fcs bypass involuntary'),
      mpatches.Patch(color='aquamarine',  label='ult total'),
      mpatches.Patch(color='dodgerblue',  label='ult involuntary'),
      Line2D([0], [0], color='yellowgreen', label='Speedup fcs'),
      Line2D([0], [0], color='orange',      label='Speedup fcs bypass'),
      Line2D([0], [0], color='blue',        label='Speedup ult')
    ]
  )

  # set labels
  fig.suptitle('Performance of Pipe test')
  fig.supxlabel('Number of Cores')
  #fig.supylabel('Number of Context Switches')
  paxis[0].set(ylabel = "Number of Context Switches")
  saxis[-1].set(ylabel = "Speedup")
  #plt.tight_layout()
  fig.savefig(f"pipe-ctx.png", bbox_inches="tight")
  # clf after using log scale triggers an unecessary warning
  #   https://github.com/matplotlib/matplotlib/issues/9970
  for ax1 in paxis:
    ax1.set_yscale("linear")
  fig.clf()

def sp(df, ver_column, metric_column, baseline_ver, params, metrics, bigger_is_better = True, sp_column = "sp"):
  df_gby = df.groupby(params)[metrics].mean().reset_index()
  versions = df_gby[ver_column].unique().tolist()
  df_bl = df_gby[df_gby[ver_column] == baseline_ver].reset_index(drop = True)
  dfv = []
  for ver in versions:
    df_ver = df_gby[df_gby[ver_column] == ver].reset_index(drop = True)
    df_ver[sp_column] = df_ver[metric_column]/df_bl[metric_column] if order else df_bl[metric_column]/df_ver[metric_column]
    dfv.append(df_ver)
  df_concat = pd.concat(dfv)
  df_merge = df_gby.merge(df_concat, on=(params + metrics))
  return df_merge

def spsd(df, ver_column, metric_column, baseline_ver, params, metrics, bigger_is_better = True, sp_column = "sp"):
  versions = df[ver_column].unique().tolist()

  dfv = []
  df_bl = df[df[ver_column] == baseline_ver].reset_index(drop = True)
  for ver in versions:
    df_ver = df[df[ver_column] == ver].reset_index(drop = True)
    df_ver[sp_column] = df_ver[metric_column]/df_bl[metric_column] if order else df_bl[metric_column]/df_ver[metric_column]
    dfv.append(df_ver)
  df_concat = pd.concat(dfv)
  df_merge = df.merge(df_concat, on=(params + metrics))

  df_gby = df_merge.groupby(params)[metrics + [sp_column]].agg(['mean', 'std']).reset_index()
  return df_gby

def runtime(df, fields):
  global df_nop_wl0, dfv

  versions = df.version.unique().tolist()
  workloads = df.workload.unique().tolist()

  df_nop_wl0 = df[(df.workload == 0) & (df.version == "nop")].reset_index(drop = True)
  df_all_wl0 = df[df.workload == 0]
  df_rpl = df_nop_wl0["time_total"] / df_nop_wl0["loops"]

  dfv = []
  for wl in workloads:
    df_nop_wlx = df[(df.version == "nop") & (df.workload == wl)].copy().reset_index(drop = True)
    df_nop_wlx["time_rt"]  = df_nop_wlx["loops"] * df_rpl
    df_nop_wlx["time_wl"]  = df_nop_wlx["time_total"] - df_nop_wlx["time_rt"]
    df_nop_wlx["time_ctx"] = 0
    dfv.append(df_nop_wlx)
    for ver in [v for v in versions if v != "nop"]: # versions not nop
      df_ver_wlx = df[(df.version == ver) & (df.workload == wl)].copy().reset_index(drop = True)
      df_ver_wlx["time_rt"]  = (df_nop_wlx["time_rt"] / df_nop_wlx["loops"]) * df_ver_wlx["loops"]
      df_ver_wlx["time_wl"]  = (df_nop_wlx["time_wl"] / df_nop_wlx["loops"]) * df_ver_wlx["loops"]
      df_ver_wlx["time_ctx"] = df_ver_wlx["time_total"] - df_ver_wlx["time_rt"] - df_ver_wlx["time_wl"]

      df_ver_wlx["per_rt"]   = (df_ver_wlx["time_rt"]  / df_ver_wlx["time_total"]) * 100
      df_ver_wlx["per_wl"]   = (df_ver_wlx["time_wl"]  / df_ver_wlx["time_total"]) * 100
      df_ver_wlx["per_ctx"]  = (df_ver_wlx["time_ctx"] / df_ver_wlx["time_total"]) * 100
      dfv.append(df_ver_wlx)

  df_concat = pd.concat(dfv)
  df_merge = df.merge(df_concat, on=(fields))
  return df_merge


def plotme(df):
  global df_pth, df_nop, df_nop_sd, df_rt, df_fcs

  params = ['version', 'pool', 'workload', 'cores', 'nthreads', 'loops']
  metrics = ["time", "lps", "spl", "ictx", "vctx", "tctx", 'time_total']

  # Set types
  df['lps']  = df['lps'].apply(Decimal)
  df['time'] = df['time'].apply(Decimal)
  df['spl']  = 1/df['lps']
  df['loops'] = 50000000
  df['time_total']  = df['spl'] * df['loops']

  # Calculate total number of context switches
  df["tctx"] = df["ictx"] + df["vctx"]


  df_rt = runtime(df, params + metrics + ["rep", "exit", "job", "exp", "id", "timeout"])
  metrics += ["time_rt", "time_ctx", "time_wl"]
  metrics += ["per_rt", "per_ctx", "per_wl"]


  plot_time_distribution(df_rt, params, metrics)

  # Calculate speedups based on both pthread and nop versions
  #df_pth = sp(df_rt, "version", "lps", "pthread", params, metrics, True)
  #df_nop = sp(df_rt, "version", "lps", "nop",     params, metrics, True)
  #df_nop_sd = spsd(df_rt, "version", "lps", "nop",     params, metrics, True)

  params = ['version', 'pool', 'workload', 'cores', 'nthreads', 'loops']
  metrics = ["time", "lps", "spl", "ictx", "vctx", "tctx", 'time_total']
  df_pth = sp(df, "version", "lps", "pthread", params, metrics, True)
  df_fcs = sp(df, "version", "lps", "fcs", params, metrics, True)


  # Combined ctx and speedup plot
  #plot_speedup_ctx(df_pth, sorted(df["workload"].unique()), "pair")

  # Per-core plot
  #gridplot_cores(df)

  # Per-workload plot
  #gridplot_workload(df)

  # Normalized on nop workload plot
  #gridplot_workload_normalized(df_nop_sd)

  #gridplot_workload_normalized2(df_nop_sd)


cdir = os.path.dirname(os.path.realpath(__file__))
os.chdir(cdir)

dftypes = {"id": str}
results = {'job-wl-tofu': {'pipe-wl-tofu': './job-wl-tofu/results_pipe-wl-tofu.csv'}}
dfs = {job: {exp: pd.read_csv(file, dtype = dftypes) for exp, file in exps.items()} for job, exps in results.items()}
df = dfs["job-wl-tofu"]["pipe-wl-tofu"]

plotme(df)


