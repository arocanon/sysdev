#!/usr/bin/env bash

function run() {
	for rep in `seq 1 3`; do
		echo "Running lockstat test $1 $2 rep $rep: ${date}"
		dir=$1_$2_$rep
		~/bin/lockstat.sh ~/bin/fcs/fcs-pipe/bin/fcs-pipe -c $1 -t 30 -n $((1024*96)) -p cycle -m $2
		mkdir $dir
		mv lockstat* $dir/
	done
}

run 0     pthread
run 10    pthread
run 100   pthread
run 1000  pthread
run 10000 pthread


run 0     fcs 
run 10    fcs 
run 100   fcs 
run 1000  fcs 
run 10000 fcs 


run 0     fcs-bypass 
run 10    fcs-bypass 
run 100   fcs-bypass 
run 1000  fcs-bypass 
run 10000 fcs-bypass 
